package ru.karimov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.karimov.tm.marker.IntegrationCategory;
import ru.t1.karimov.tm.client.IProjectEndpointClient;
import ru.t1.karimov.tm.model.Project;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Category(IntegrationCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProjectRestEndpointTest {

    @NotNull
    final static String BASE_URL = "http://localhost:8080/api/projects";

    @NotNull
    private final IProjectEndpointClient projectEndpointClient = IProjectEndpointClient.projectClient(BASE_URL);

    @NotNull
    private final Project projectTest1 = new Project("Project Test 1");

    @NotNull
    private final Project projectTest2 = new Project("Project Test 2");

    @NotNull
    private final Project projectTest3 = new Project("Project Test 3");

    @NotNull
    private final Project projectTest4 = new Project("Project Test 4");

    @NotNull
    private Collection<Project> projects = new ArrayList<>();

    @Before
    public void init() {
        projectEndpointClient.save(projectTest1);
        projectEndpointClient.save(projectTest2);
        projectEndpointClient.save(projectTest3);
        projects = projectEndpointClient.findAll();
    }

    @After
    public void clean() {
        projectEndpointClient.delete(projectTest1);
        projectEndpointClient.delete(projectTest2);
        projectEndpointClient.delete(projectTest3);
        projects.clear();
    }

    @Test
    public void findById() {
        for (@NotNull final Project project : projects) {
            @NotNull final String id = project.getId();
            @Nullable final Project actualProject = projectEndpointClient.findById(id);
            assertNotNull(actualProject);
            assertEquals(id, actualProject.getId());
        }
    }

    @Test
    public void findAll() {
        @NotNull final Collection<Project> actualProjects = projectEndpointClient.findAll();
        assertEquals(projects.size(), actualProjects.size());
    }

    @Test
    public void delete() {
        for (@NotNull final Project project : projects) {
            projectEndpointClient.delete(project);
        }
        @NotNull final Collection<Project> actualProjects = projectEndpointClient.findAll();
        assertEquals(0, actualProjects.size());
    }

    @Test
    public void deleteById() {
        for (@NotNull final Project project : projects) {
            @NotNull final String id = project.getId();
            projectEndpointClient.delete(id);
        }
        @NotNull final Collection<Project> actualProjects = projectEndpointClient.findAll();
        assertEquals(0, actualProjects.size());
    }

    @Test
    public void save() {
        projectEndpointClient.save(projectTest4);
        @NotNull final String expectedId = projectTest4.getId();
        @NotNull final String expectedName = projectTest4.getName();
        @Nullable final Project actualProject = projectEndpointClient.findById(expectedId);
        assertNotNull(actualProject);
        assertEquals(expectedName, actualProject.getName());

        projectEndpointClient.delete(projectTest4);
    }

}
