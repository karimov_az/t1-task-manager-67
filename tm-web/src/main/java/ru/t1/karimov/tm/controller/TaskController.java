package ru.t1.karimov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Project;
import ru.t1.karimov.tm.model.Task;
import ru.t1.karimov.tm.service.ProjectService;
import ru.t1.karimov.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    private Collection<Project> getProjects() {
        return projectService.findAll();
    }

    @NotNull
    @GetMapping("/task/create")
    public String create() {
        taskService.add(new Task("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") final String id
    ) {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @NotNull
    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") final Task task,
            BindingResult result
    ) {
        taskService.add(task);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) {
        @Nullable final Task task = taskService.findOneById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects());
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
