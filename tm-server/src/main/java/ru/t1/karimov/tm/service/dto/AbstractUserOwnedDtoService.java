package ru.t1.karimov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.karimov.tm.comparator.CreatedComparator;
import ru.t1.karimov.tm.comparator.StatusComparator;
import ru.t1.karimov.tm.dto.model.AbstractUserOwnedDtoModel;
import ru.t1.karimov.tm.exception.entity.EntityNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.IndexIncorrectException;
import ru.t1.karimov.tm.exception.field.UserIdEmptyException;
import ru.t1.karimov.tm.repository.dto.IUserOwnedDtoRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedDtoModel>
        extends AbstractDtoService<M> implements IUserOwnedDtoService<M> {

    @NotNull
    private static final String COLUMN_CREATED = "created";

    @NotNull
    private static final String COLUMN_STATUS = "status";

    @NotNull
    private static final String COLUMN_NAME = "name";

    @NotNull
    @Autowired
    protected IUserOwnedDtoRepository<M> repository;

    @NotNull
    protected String getSortType(@NotNull final Comparator<?> comparator) {
        if (comparator == CreatedComparator.INSTANCE) return COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return COLUMN_STATUS;
        else return COLUMN_NAME;
    }

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        return repository.save(model);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(final @Nullable String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull final String sort = getSortType(comparator);
        return repository.findAllByUserIdAndSort(userId, sort);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        return repository.findByUserId(userId, PageRequest.of(index,1))
                .stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        if (!userId.equals(model.getUserId())) throw new EntityNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        @NotNull final M model = Optional.ofNullable(findOneByIndex(userId, index)).orElseThrow(EntityNotFoundException::new);
        repository.delete(model);
    }

    @Override
    @Transactional
    public void update(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        if (!userId.equals(model.getUserId())) throw new EntityNotFoundException();
        repository.save(model);
    }

}
