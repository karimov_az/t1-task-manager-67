package ru.t1.karimov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    Project changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable Status status
    );

    @NotNull
    Project updateProjectById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Project updateProjectByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

}
