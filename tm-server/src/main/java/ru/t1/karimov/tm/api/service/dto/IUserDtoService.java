package ru.t1.karimov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.model.UserDto;
import ru.t1.karimov.tm.enumerated.Role;

public interface IUserDtoService extends IDtoService<UserDto> {

    @NotNull
    UserDto create(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    UserDto create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    UserDto create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    UserDto findByLogin(@Nullable String login);

    @Nullable
    UserDto findByEmail(@Nullable String email);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    void removeOneByLogin(@Nullable String login);

    void removeOneByEmail(@Nullable String email);

    @NotNull
    UserDto setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDto updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void unlockUserByLogin(@Nullable String login);

}
