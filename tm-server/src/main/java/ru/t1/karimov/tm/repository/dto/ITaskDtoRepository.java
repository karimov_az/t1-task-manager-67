package ru.t1.karimov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.karimov.tm.dto.model.TaskDto;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDto> {

    @NotNull
    List<TaskDto> findByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
